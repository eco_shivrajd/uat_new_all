<!-- BEGIN HEADER -->
<?php include "../includes/header.php"?>
<!-- END HEADER -->
<?php
if(isset($_POST['submit']))
{
	$id=$_GET['id'];
	$cmbSuperStockist=$_POST['cmbSuperStockist'];
	if($cmbSuperStockist!=""){
		$assign=$cmbSuperStockist;
	}
	else
		$assign=$_POST['assign'];
	$name=$_POST['firstname'];
	$address=$_POST['address'];
	$state=$_POST['state'];
	$city=$_POST['city'];
	$email=$_POST['email'];
	$mobile=$_POST['mobile'];
	$sqlu="SELECT * FROM tbl_user where id='".$id."'";
	$resultu = mysqli_query($con,$sqlu);
	$rowu = mysqli_fetch_array($resultu);
	$update_sql=mysqli_query($con,"UPDATE tbl_user SET external_id='$assign' where id='$id'");

	//Insert in main table CompanyNM
	$sql1="SELECT * FROM tbl_users where id='".$id."'";
	$result1 = mysqli_query($conmain,$sql1);
	if(mysqli_num_rows($result1)>0){
		//update
		$update_sql=mysqli_query($conmain,"UPDATE tbl_users SET `emailaddress`='$email',`passwd`='".$rowu['pwd']."',`username`='".$rowu['username']."',`level`='".$rowu['user_type']."' where id='$id'");
		$sql1="SELECT * FROM tbl_user_company where userid='".$id."' AND companyid='".COMPID."'";
		$result1 = mysqli_query($conmain,$sql1);
		if(mysqli_num_rows($result1)==0){
			//COMPID
			$sql1 = mysqli_query($conmain,"INSERT INTO tbl_user_company(userid,companyid) 
			VALUES('".$id."','".COMPID."')");
		}
	}
	else
	{
		$sql1 = mysqli_query($conmain,"INSERT INTO tbl_users (`id`, `emailaddress`, `passwd`, `username`, `level`) 
		VALUES('".$id."','".$email."','".$rowu['pwd']."','".$rowu['username']."','".$rowu['user_type']."')");
		$sql1="SELECT * FROM tbl_user_company where userid='".$id."' AND companyid='".COMPID."'";
		$result1 = mysqli_query($conmain,$sql1);
		if(mysqli_num_rows($result1)==0){
			//COMPID
			$sql1 = mysqli_query($conmain,"INSERT INTO tbl_user_company(userid,companyid) 
			VALUES('".$id."','".COMPID."')");
		}
	}
	//header('location:sales.php');
	echo '<script>location.href="sales.php";</script>';
}
?>
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "SalesPerson";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- /.modal -->
			<h3 class="page-title">Sales Person</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="javascript:;">Manage</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="sales.php">Sales Person</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Edit Sales Person</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Edit Sales Person
							</div>
						</div>
						<div class="portlet-body">
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
					    <?php
						//Get Distributorid
						$id			= $_GET['id'];
						$sql1		= "SELECT * FROM tbl_user where id = '$id' ";
						$result1 	= mysqli_query($con,$sql1);
						$row1 		= mysqli_fetch_array($result1);
						
						//Get parent of this distributor
						$sqlp		= "SELECT external_id FROM tbl_user where id='".$row1['external_id']."'";
						$resultp 	= mysqli_query($con,$sqlp);
						$rowp 		= mysqli_fetch_array($resultp);
						$parentid	= $rowp['external_id'];
						?>                       

					<form class="form-horizontal" role="form" data-parsley-validate="" action="" method="post">
						
						<? if($_SESSION[SESSION_PREFIX."user_type"]=="Admin") { ?>
						<div class="form-group">
						<label class="col-md-3">Super Stockist:</label>
						<div class="col-md-4">
							<select name="cmbSuperStockist" id="cmbSuperStockist" onchange="fnShowStockist(this)" class="form-control">
								<option value="">-Select-</option>
								<? 
								$user_type="Superstockist";
								$sql="SELECT firstname,id FROM `tbl_user` where user_type ='$user_type'";
								$result1 = mysqli_query($con,$sql); 
								while($row = mysqli_fetch_array($result1)) 
								{
									$assign_id	=	$row['id'];
									if($parentid == $assign_id)
										$sel="SELECTED";
									else
										$sel="";
									echo "<option value='$assign_id' $sel>" . $row['firstname'] . "</option>";
								} ?>
							</select>
						</div>
						</div><!-- /.form-group -->    
						<? } else { ?>
							<input type="hidden" name="cmbSuperStockist" id="cmbSuperStockist" value="<?=$parentid;?>">
						<? } ?>					
						
						<? if($_SESSION[SESSION_PREFIX."user_type"]!="Distributor") { ?>
						<div id="Subcategory">
							<div class="form-group">
								<label class="col-md-3">Stockist:</label>
								<div class="col-md-4">
									<select name="assign" class="form-control">
									<option value="">-Select-</option>
									<?php
									$user_type="Distributor";
									$sql="SELECT firstname , id FROM `tbl_user` WHERE user_type ='$user_type'";
									if($parentid!="")
										$sql .= " AND external_id = $parentid ";
									$result1 = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result1))
									{
										$assign_id=$row['id'];
										if($row1['external_id'] == $assign_id)
											$sel="SELECTED";
										else
											$sel="";
										echo "<option value='$assign_id' $sel>" . $row['firstname'] . "</option>";
									} ?>
									</select>
								</div>
							</div><!-- /.form-group -->  
						</div>
						<? } else {?>
							<input type="hidden" name="assign" id="assign" value="<?=$row1['external_id'];?>">
						<? } ?>
					  
						<div class="form-group">
						  <label class="col-md-3">Name:</label>
						  <div class="col-md-4">
							<input type="text" name="firstname" 
							placeholder="Enter Name"
							 class="form-control" value="<?php echo $row1['firstname']?>" readonly>
						  </div>
						</div><!-- /.form-group -->
						
						<div class="form-group">
						  <label class="col-md-3">Address: </label>
						  <div class="col-md-4">
							<textarea name="address" rows="4"
							placeholder="Enter Address"
							 
							class="form-control" readonly><?php echo $row1['address']?></textarea>
						  </div>
						</div><!-- /.form-group -->
						<div class="form-group">
						  <label class="col-md-3">State:</label>
						  <div class="col-md-4">
						  <select name="state"
						   
						  class="form-control" readonly>
							<?php
							$sql="SELECT * FROM tbl_state where id='".$row1['state']."'";
							$result = mysqli_query($con,$sql);
							 
							if(mysqli_num_rows($result)>0){
								$row = mysqli_fetch_array($result);
								$cat_id=$row['id'];
								echo "<option value='$cat_id'>" . $row['name'] . "</option>"; 
							}
							?>
							</select>
						  </div>
						</div><!-- /.form-group -->
						
						<div class="form-group">
						  <label class="col-md-3">City:</label>

						  <div class="col-md-4">
						  <select name="city"
						  
						  class="form-control" readonly>
							<?php
							$state=$row1['state'];
							$sql	= "SELECT * FROM tbl_city where id='".$row1['city']."' ORDER BY name";
							$result = mysqli_query($con,$sql);
							$row 	= mysqli_fetch_array($result);
							$cat_id = $row['id'];
							echo "<option value='$cat_id'>" . $row['name'] . "</option>";
							?>
							</select>
						  </div>
						</div><!-- /.form-group --> 
			 
					   <div class="form-group">
						  <label class="col-md-3">Suburb:</label>

						  <div class="col-md-4">
						  <select name="suburbnm" class="form-control" readonly disabled >
						  <option selected disabled>-select-</option>
							<?php
							$sql="SELECT * FROM tbl_surb";
							$result = mysqli_query($con,$sql);
							while($row = mysqli_fetch_array($result))
							{
								$cat_id=$row['id'];
								if($row1['suburbid'] == $cat_id)
									$sel="SELECTED";
								else
									$sel="";
								echo "<option value='$cat_id' $sel>" . $row['suburbnm'] . "</option>";
							} ?>
							</select>
						  </div>
						</div><!-- /.form-group -->          
						<div class="form-group">
						  <label class="col-md-3">Email:</label>

						  <div class="col-md-4">
							<input type="text" name="email" 
							  placeholder="Enter E-mail"
							   
							class="form-control" readonly value="<?php echo $row1['email']?>">
						  </div>
						</div><!-- /.form-group -->
						
						
						<div class="form-group">
						  <label class="col-md-3">Mobile Number: </label>

						  <div class="col-md-4">
							<input type="text" name="mobile" 
							placeholder="Enter Mobile Number"
							 
							class="form-control" value="<?php echo $row1['mobile']?>" readonly>
						  </div>
						</div><!-- /.form-group -->

						<div class="form-group">
							<div class="col-md-4 col-md-offset-3">
								<? if($_SESSION[SESSION_PREFIX."user_type"]!="Distributor") { ?>
								<button name="submit" id="submit" class="btn btn-primary">Submit</button>
								<? } ?>
								<a href="sales.php" class="btn btn-primary">Cancel</a>
							</div>
						</div><!-- /.form-group -->
					</form>  
				   <div class="modal fade" id="thankyouModal" tabindex="-1" role="dialog" aria-labelledby="thankyouLabel" aria-hidden="true">
					<div class="modal-dialog" style="width:300px;">
						<div class="modal-content">
							<div class="modal-body">
								<p>
								<h4 style="color:red; text-align:center;">Do you want to delete this record ?</h4>
								</p>                     
							  <center><a href="sales_delete.php?id=<?php echo $row1['id']?>" ><button type="button" class="btn btn-success">Yes</button></a>
							  <button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
							  </center>
							</div>    
						</div>
					</div>
					</div>  
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
<!-- END PAGE LEVEL SCRIPTS -->
<script>
function fnShowStockist(id){
if (window.XMLHttpRequest)
{
xmlhttp=new XMLHttpRequest();
}
else
{
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("Subcategory").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("GET","fetchstockist.php?cat_id="+id.value,true);
xmlhttp.send();
 }
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>