
ALTER TABLE `tbl_order_details` ADD `free_product_details` VARCHAR(1000) NOT NULL COMMENT 'brand_id,brand_name,category_id,category_name,product_id,product_name,product_varid,weight,quantity,price' AFTER `campaign_sale_type`;

ALTER TABLE `tbl_shop_visit` ADD `no_o_lat` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `shop_visit_date_time`, ADD `no_o_long` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `no_o_lat`;

ALTER TABLE `tbl_sp_attendance` ADD `todays_travelled_distance` DECIMAL(10,2) NOT NULL AFTER `endlongg`;